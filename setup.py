from setuptools import setup

setup(
  name='tello-formation',
  version='0.1-prealpha',
  description='For teaching younsters flying Tello in formation through Python Programming',
  author='hulloanson',
  author_email='hulloanson@gmail.com',
  license='MIT',
  packages=['tello-formation'],
  install_requires=[
    'netifaces',
    'netaddr',
    'python-dotenv',
  ],
  zip_safe=False
)