tellos = [
'0TQDG7BEDBJ740'
'0TQDG38EDBDW1U'
'0TQDG7KEDB306P'
'0TQDG38EDB041Y'
]

commands = [

]

def do(command, args='', devices=None):
  commandBody = '%s %s' % (command, args.join(' '))
  if isinstance(devices, list):
    for device in devices:
      commands.append()
  elif isinstance(devices, str) and devices == '*':
      commands.append('*>%s' % (commandBody))
  else:
    raise Exception('devices should be list or the string *(asterisk)')

  commands.append()

def compile():
  program = ''
  for command in commands:
    program += command
  with open('outputs/script.txt', mode='w') as f:
    f.write(program)
