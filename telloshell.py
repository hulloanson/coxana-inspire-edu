import threading
import socket
import sys
import time
from string import Template


class TelloController:
    def __init__(self, ctlrhost='', ctlrport=9000,
                 tellohost='192.168.10.1', telloport=8889):
        self.telloaddr = (tellohost, telloport)
        self.locaddr = (ctlrhost, ctlrport)
        self.run = False
        self.sock = None
        self.recvThread = None
        self.inputThread = None

    def connect(self):
        print('Connecting to Tello...')
        retriesleft = 3
        while True:
            self.sock.sendto('command'.encode(encoding="utf-8"),
                             self.telloaddr)
            try:
                if 'ok' not in self.recvfrom():
                    print('Tello didn\'t respond correctly.')
                    self.closesock()
                    exit(1)
            except socket.timeout:
                if retriesleft == 0:
                    print('''Retries used up. Still couldn\'t connect to Tello.
Please make sure the Tello is reachable.''')
                    self.closesock()
                    exit(1)
                retriesleft -= 1
                print(Template('Couldn\'t connect to Tello. $n retries left.')
                      .substitute(n=retriesleft))
                continue
            break

    def setupsock(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.bind(self.locaddr)
        self.sock.settimeout(5)

    def start(self):
        print('''Tello: command takeoff land flip forward back left right
up down cw ccw speed speed?\r\n''')
        print('end -- quit demo.\r\n')

        try:
            self.setupsock()
            self.connect()
            self.run = True
            self.recvThread = threading.Thread(target=self.recv)
            self.inputThread = threading.Thread(target=self.getinput)
            self.recvThread.start()
            self.inputThread.start()
            self.wait()
            self.closesock()
            print('Bye bye :)')
        except KeyboardInterrupt:
            print('Type \'end\' to quit instead')

    def wait(self):
        recvDone = self.recvThread is None
        inputDone = self.inputThread is None
        while not (recvDone and inputDone):
            if not inputDone:
                self.inputThread.join(timeout=0.1)
                if not self.inputThread.isAlive():
                    inputDone = True
                    self.inputThread = None

            if not recvDone:
                self.recvThread.join(timeout=0.1)
                if not self.recvThread.isAlive():
                    recvDone = True
                    self.recvThread = None

    def stop(self):
        print('Waiting until everything shuts down. You may have to wait for a'
              ' few seconds')
        self.run = False

    def closesock(self):
        if self.sock is not None:
            self.sock.close()

    def recv(self):
        while self.run:
            try:
                data = self.recvfrom()
                print('received: \r\n', data)
            except socket.timeout:
                continue
            except Exception as e:
                print('recv thread exited due to an exception. Exiting...')
                print('Except was ', type(e))
                self.stop()

    def recvfrom(self):
        data, _ = self.sock.recvfrom(1518)
        return data.decode(encoding="utf-8")

    def getinput(self):
        while self.run:
            msg = input("tello> ")

            if 'end' in msg:
                self.stop()
                break
            if not msg:
                continue

            # Send data
            msg = msg.encode(encoding="utf-8")
            print('Sending...')
            sent = self.sock.sendto(msg, self.telloaddr)
            print(Template('Sent $n bytes.').substitute(n=sent))


ctlr = TelloController()
ctlr.start()
